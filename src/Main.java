import java.util.List;
import java.util.Scanner;

public class Main {
    static FirstDictionary firstDictionary;
    static SecondDictionary secondDictionary;
    static Dictionary currentDictionary;

    private static final List<String> menu = List.of(
            "Выберите операцию со словарём: ",
            "1. Выбрать словарь (1/2)",
            "2. Вывести словарь на экран",
            "3. Добавить ключ",
            "4. Удалить ключ",
            "5. Найти ключ",
            "6. Вывести оба словаря на экран",
            "7. Выход");

    private static final List<String> dictionaryMenu = List.of(
            "Выберите словарь: ",
            "1. Словарь с латинскими буквами" ,
            "2. Словарь с арабскими цифрами");

    public static void main(String[] args) {

        while (true){
            menu.forEach(System.out::println);
            Scanner in1 = new Scanner(System.in);
            Scanner in2 = new Scanner(System.in);
            try {
                String operation = in1.next();
                switch (operation) {
                    case "1": {
                        dictionaryMenu.forEach(System.out::println);
                        String changeDictionary = in2.next();
                        switch (changeDictionary) {
                            case "1": {
                                System.out.println("Введите название файла: ");
                                String filename = in1.next();
                                FirstDictionary temp = new FirstDictionary(filename);
                                if (!temp.isEmpty()) {
                                    currentDictionary = temp;
                                    firstDictionary = temp;
                                }
                                break;
                            }
                            case "2": {
                                System.out.println("Введите название файла: ");
                                String filename = in1.next();
                                SecondDictionary temp = new SecondDictionary(filename);
                                if (!temp.isEmpty()) {
                                    currentDictionary = temp;
                                    secondDictionary = temp;
                                }
                                break;
                            }
                            default: {
                                System.out.println("Ошибка ввода!");
                                break;
                            }
                        }
                        break;
                    }
                    case "2": {
                        if (currentDictionary == null) {
                            System.out.println("Сначала выберите словарь!");
                        } else {
                            currentDictionary.Output();
                        }
                        break;
                    }
                    case "3": {
                        if (currentDictionary == null) {
                            System.out.println("Сначала выберите словарь!");
                        } else {
                            in2 = new Scanner(System.in);
                            System.out.println("Введите слово: ");
                            String key = in1.next();
                            System.out.println("Введите перевод(ы) слова через пробел: ");
                            String value = in2.nextLine();
                            currentDictionary.addKey(key, value);
                            currentDictionary.saveToFile();
                        }
                        break;
                    }
                    case "4": {
                        if (currentDictionary == null) {
                            System.out.println("Сначала выберите словарь!");
                        } else {
                            System.out.println("Введите ключ, который хотите удалить: ");
                            currentDictionary.removeKey(in1.next());
                            currentDictionary.saveToFile();
                        }
                        break;
                    }
                    case "5": {
                        if (currentDictionary == null) {
                            System.out.println("Сначала выберите словарь!");
                        } else {
                            System.out.println("Введите ключ, который хотите найти: ");
                            System.out.println(currentDictionary.contains(in1.next()));
                        }
                        break;
                    }
                    case "6": {
                        if (firstDictionary != null) {
                            System.out.println("Словарь 1: ");
                            firstDictionary.Output();
                        }
                        if (secondDictionary != null) {
                            System.out.println("Словарь 2: ");
                            secondDictionary.Output();
                        }
                        break;
                    }
                    case "7": {
                        System.exit(0);
                        break;
                    }
                    default: {
                        System.out.println("Такой команды не существует!\n");
                        break;
                    }
                }
                updateDictionary();
            }
            catch (Exception e){
                System.out.println(e.getMessage() + "\n");
            }
        }
    }

    private static void updateDictionary() {
        if (currentDictionary instanceof FirstDictionary) {
            firstDictionary = (FirstDictionary) currentDictionary;
        }
        else {
            secondDictionary = (SecondDictionary) currentDictionary;
        }
    }
}