import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileReader;
import java.util.Map;
import java.io.IOException;
import java.util.HashMap;

public abstract class Dictionary implements IDictionary {

    private final HashMap<String, String> myDictionary = new HashMap<>();
    protected abstract String getKey();
    private final String filename;

    public Dictionary(String filename) {
        this.filename = filename;
        readFile();
    }

    public void readFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] mas = line.split(" ");
                String key = mas[0];
                StringBuilder builder = new StringBuilder();
                for (int i = 1; i < mas.length; i++){
                    builder.append(mas[i] + " ");
                }
                myDictionary.put(key, builder.toString());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void saveToFile() {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename))) {
            for (Map.Entry<String, String> entry : myDictionary.entrySet()) {
                bw.write(entry.getKey() + " " + entry.getValue());
                bw.newLine();
            }
        } catch (IOException e) {
            System.out.println("Ошибка при сохранении в файл: " + e.getMessage());
        }
    }

    public void Output() {
        if (myDictionary.isEmpty()) {
            System.out.println("Словарь пустой");
        } else {
            myDictionary.forEach((key, value) -> {
                String[] words = value.split(" ");
                System.out.println(key + ": ");
                for (String word : words) {
                    System.out.println("\t -> " + word);
                }
            });
        }
    }

    public void addKey(String key, String value) {
        if (!key.matches(getKey())) {
            throw new RuntimeException("Ключ не соответствует требованиям словаря");
        }
        myDictionary.put(key, value);
    }

    public void removeKey(String key) {
        if (!myDictionary.containsKey(key)) {
            throw new RuntimeException("Такого ключа нет в словаре");
        }
        myDictionary.remove(key);
    }

    public String contains(String key) {
        if (!myDictionary.containsKey(key)) {
            throw new RuntimeException("Такого ключа нет в словаре");
        }
        return myDictionary.get(key);
    }

    public Boolean isEmpty() {
        return myDictionary.isEmpty();
    }
}
