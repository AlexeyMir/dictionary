public class SecondDictionary extends Dictionary{

    public SecondDictionary(String fileName) {super(fileName);}

    @Override
    protected String getKey() {
        return "[0-9]{5}";
    }
}
