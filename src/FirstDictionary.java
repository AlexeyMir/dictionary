public class FirstDictionary extends Dictionary{

    public FirstDictionary(String fileName) {super(fileName);}

    @Override
    protected String getKey() {
        return "[a-zA-Z]{4}";
    }
}
