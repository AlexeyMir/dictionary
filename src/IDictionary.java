public interface IDictionary {

    void addKey(String key, String value);

    void removeKey(String key);

    String contains(String key);
}
